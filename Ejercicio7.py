'''
Created on 4/10/2021

@author: Herrera
'''

class CadenaInversa:
    def cadenaInversa(self, cadena, lenCadena):
        if(lenCadena<0):
            return ""
        else:
            return f"{cadena[lenCadena]}{CadenaInversa().cadenaInversa(cadena, lenCadena-1)}"


print("Programa que muestre una cadena en forma inversa")
cadena = input("Introduce cadena: ")
ci = CadenaInversa()

print("Cadena Inversa: ")
print(ci.cadenaInversa(cadena, len(cadena)-1))