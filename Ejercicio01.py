'''
Created on 4/10/2021

@author: Herrera
'''
class Divisores:
    def mostrarDivisoresNumero(self, numero, n):
        if(n >= numero):
            print("Fin del programa")
        else:
            if((numero % n) == 0):
                print(n)
            Divisores().mostrarDivisoresNumero(numero, (n+1))
    
print("Programa que muestre los DIVISORES de un numero dado")
d1 = Divisores()
numero = int(input("Introduce numero: "))
print("Divisores del numero: ")

d1.mostrarDivisoresNumero(numero, 2)