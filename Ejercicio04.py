'''
Created on 4/10/2021

@author: Herrera
'''
class DivisoresEnteros:
    def divisoresEnteros(self, a, b):
        if(b>=a):
            return 0
        else:
            if((a%b)==0):
                return 1 + DivisoresEnteros().divisoresEnteros(a, b+1)
            else:
                return 0 + DivisoresEnteros().divisoresEnteros(a, b+1)

print("Programa que muestre cuantos son los divisores enteros entre dos numeros dados")

a = int(input("Introduce primer numero: "))
b = int(input("introduce segundo numero: "))

de = DivisoresEnteros()

print("Cantidad de divisores enteros: ")
print(de.divisoresEnteros(a, b))