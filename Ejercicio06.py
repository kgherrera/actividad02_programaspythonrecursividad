'''
Created on 4/10/2021

@author: Herrera
'''
class Vocales:
    def cantidadVocales(self, cadena, letras):
        if(letras == len(cadena)):
            return 0;
        else:
            if(cadena[letras] == "a"):
                return 1 + Vocales.cantidadVocales(self, cadena, letras+1)
            elif (cadena[letras] == "e"):
                return 1 + Vocales.cantidadVocales(self, cadena, letras+1)
            elif (cadena[letras] == "i"):
                return 1 + Vocales.cantidadVocales(self, cadena, letras+1)
            elif (cadena[letras] == "o"):
                return 1 + Vocales.cantidadVocales(self, cadena, letras+1)
            elif (cadena[letras] == "u"):
                return 1 + Vocales.cantidadVocales(self, cadena, letras+1)
            else:
                return  0 + Vocales.cantidadVocales(self, cadena, letras+1)
        
print("Programa que calcule a cantidad de vocales en una cadena")
cadena = input("Introduce cadena: ")

v = Vocales()

print("Cantidad vocales: ")
print(v.cantidadVocales(cadena, 0))