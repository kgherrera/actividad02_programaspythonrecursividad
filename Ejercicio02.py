'''
Created on 4/10/2021

@author: Herrera
'''
class Factorial:
    def factorial(self, numero):
        if(numero==0): 
            return 1;
        else: 
            return numero * Factorial().factorial(numero-1)
        
print("Programa que muestre el FACTORIAL de un numero dado")
f1 = Factorial()
numero = int(input("Introduce numero: "))

print("Factorial del numero: ")
print(f1.factorial(numero))